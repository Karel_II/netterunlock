<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteRunLock\Run;

/**
 * Description of Loop
 *
 * @author karel.novak
 */
class Loop {

    /**
     * @var int 
     */
    private $microtime = NULL;

    /**
     * @var int 
     */
    private $windowTime = NULL;

    /**
     * @var type 
     */
    private $windowTimeMax = NULL;

    /**
     * @param integer $windowTime
     */
    function __construct($windowTime, $windowTimeMax = NULL) {
        $this->windowTime = $windowTime;
        $this->windowTimeMax = $windowTimeMax;
    }

    public function loopStart() {
        $this->microtime = microtime(true);
    }

    public function loopSleep() {
        $microtime = microtime(true);
        $sleepTime = $this->windowTime - intval(($microtime - $this->microtime) * 1000000);
        if ($sleepTime > 0) {
            usleep($sleepTime);
            if (\Tracy\Debugger::isEnabled()) {
                \Tracy\Debugger::dump(sprintf("RUN - %.3f ms, %.3f ms", ((microtime(true) - $this->microtime ) * 1000), ($sleepTime / 1000)));
            }
        } else {
            if (\Tracy\Debugger::isEnabled()) {
                \Tracy\Debugger::dump(sprintf("RUN - %.3f ms, -------", (( microtime(true) - $this->microtime ) * 1000)));
            }
        }
        if ($this->windowTimeMax !== NULL && (( microtime(true) - $this->microtime ) * 1000000) > $this->windowTimeMax) {
            throw new \Exception('Max Run Time Exceeded !!');
        }
    }

}
