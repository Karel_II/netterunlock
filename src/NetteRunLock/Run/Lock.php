<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteRunLock\Run;

use \Nette\Application\UI\Presenter,
    \Nette\Utils\FileSystem;

/**
 * Description of newPHPClass
 *
 * @author Karel_II
 */
class Lock {

    /** @var string */
    private $tempLock;

    /** @var string */
    private $lockFile;

    /**
     * 
     * @param string $temp
     */
    public function __construct($temp) {
        $this->tempLock = $temp . DIRECTORY_SEPARATOR . 'lock' . DIRECTORY_SEPARATOR;
        FileSystem::createDir($this->tempLock);
    }

    public function setLock(Presenter $presenter) {
        $this->lockFile = $this->tempLock . ucfirst($presenter->name) . '_' . ucfirst($presenter->action);
        if (!$this->tryLock()) {
            die("Already running.\n");
        }
        register_shutdown_function('unlink', $this->lockFile);
    }

    private function tryLock() {
        # If lock file exists, check if stale.  If exists and is not stale, return TRUE
        # Else, create lock file and return FALSE.

        if (@symlink(DIRECTORY_SEPARATOR . 'proc' . DIRECTORY_SEPARATOR . getmypid(), $this->lockFile) !== FALSE) # the @ in front of 'symlink' is to suppress the NOTICE you get if the LOCK_FILE exists
            return true;

        # link already exists
        # check if it's stale
        if (is_link($this->lockFile) && !is_dir($this->lockFile)) {
            unlink($this->lockFile);
            # try to lock again
            return $this->tryLock();
        }

        return false;
    }

}
