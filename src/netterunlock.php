<?php

spl_autoload_register(function ($className) {
    static $classMap = [
        'NetteRunLock\\Run\\Lock' => 'RunLock/Menu.php',
    ];
    if (isset($classMap[$className])) {
        require __DIR__ . '/NetteRunLock/' . $classMap[$className];
    }
});
